# wwex-aws-test

## Purpose
To establish a testing framework for resources created in AWS.

## Audience
WWEX Engineers building resources

## Goals
Establish a framework for testing.

  * path - a place to put the tests, maybe matching terraform paths
  * examples - example ruby code for running tests
  * configuration - a testing user with priviledge to test, least priviledge protecting secrets
